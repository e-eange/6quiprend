#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "game.h"
#include "interfaceTerm.h"

/* Auteur : Hugo Dury */
/* Date : 12/06/23 */
/* Résumé : starter du jeu et initialise le nb de joueur (physique et IA) */
/* Entrée(s) : le nb de joueur et d'ia en pointeur */
/* Sortie(s) : aucune */
void intro(int* nbJoueur, int* nbIa){
	system("clear");
	printf("   _____                _                              __   __\n  / ___/   ____ ___  __(_)  ____  ________  ____  ____/ /  / /\n / __ \\   / __ `/ / / / /  / __ \\/ ___/ _ \\/ __ \\/ __  /  / / \n/ /_/ /  / /_/ / /_/ / /  / /_/ / /  /  __/ / / / /_/ /  /_/  \n\\____/   \\__, /\\__,_/_/  / .___/_/   \\___/_/ /_/\\__,_/  (_)   \n           /_/          /_/                                   ");
	printf("\n\nBy Celia Candy, Hugo Dury and Etienne Angé");

	printf("\n\n\n\n\n nombre de joueur (1-10) : ");
	scanf("%d", nbJoueur);
	while(*nbJoueur < 1 || *nbJoueur > 10){
		printf("Veuillez entrer un entier entre 1 et 10 : ");
		scanf("%d", nbJoueur);
	}

	int min;
	if(*nbJoueur == 1){min=1;}else{min=0;}

	if(*nbJoueur != 10){
		printf("\n\n\n\n\n nombre d'IA (%d-%d) : ", min, (10-*nbJoueur));
		scanf("%d", nbIa);
		while(*nbIa < min || *nbIa > 10-*nbJoueur){
			printf("Veuillez entrer un entier entre %d et %d : ", min, (10-*nbJoueur));
			scanf("%d", nbIa);
		}}
	else{
		*nbIa = 0;
		}
}

/* Auteur : Celia Candy */
/* Date : 13/06/23 */
/* Résumé : demande le nom des joueurs */
/* Entrée(s) : le nb de joueur et d'ia, et le tableau de joueur */
/* Sortie(s) : aucune */
void pseudo(int nbJoueur,int nbIa, struct joueur *joueurs){
	system("clear");
	for(int i=0;i<nbJoueur;i++){
		printf("\n             ★ PLAYER %d ★", i+1);
		printf("\n          écris ton pseudo : ");
		scanf("%s", joueurs[i].nom);
		joueurs[i].ia = 0;
	}
	for(int j=nbJoueur;j<nbIa+nbJoueur;j++){
		joueurs[j].nom[0] = 'I';
		joueurs[j].nom[1] = 'A';
		joueurs[j].nom[2] = ' ';
		joueurs[j].nom[3] = 49+j-nbJoueur;
		joueurs[j].nom[4] = '\0';
		joueurs[j].ia = 1;
	}

}


/* Auteur : Etienne Angé */
/* Date :   14/06/2023*/
/* Résumé : affiche le plateau de jeu*/
/* Entrée(s) : le tableau du plateau */
/* Sortie(s) :  aucune  */
void showGameBoard(int** plateau){


	printf("\n\n\n");
	for(int i=0; i<5; i++){
		printf("             ");
		for(int j=0;j<5;j++){
			if(plateau[i][j] == 0){
				printf("_");
			}
			else{
				printf("%d", plateau[i][j]);
			}

			if((plateau[i][j] / 10) > 0 && (plateau[i][j] / 10 < 10)){
				printf(" ");
			}
			else if(plateau[i][j] / 10 == 0){
				printf("  ");
			}
			if(j != 4){
				printf("  ");
			}
		}
		printf("\n");
	}
}

/* Auteur : Hugo Dury */
/* Date : 15/06/23 */
/* Résumé : fonction qui affiche les cartes d'un joueur */
/* Entrée(s) : la variable joueur du type struct joueur */
/* Sortie(s) : aucune */
void showHand(struct joueur player){
	printf("\n\n\nJoueur : %s", player.nom);
	printf("\nmains : ");
	for(int j=0;j<10;j++){
			if(player.cartes[j] != 0){
				printf(" %d", player.cartes[j]);
			}

	}
	printf("\n");
}


/* Auteur : Celia Candy */
/* Date : 16/06/23 */
/* Résumé : affiche le nb de tdb de chaque joueur */
/* Entrée(s) : le nb de joueur et le tableau des joueurs */
/* Sortie(s) : aucune */
void showTDB(int nbjoueur, struct joueur *joueurs){
	system("clear");
	printf("\n\n---Têtes de boeufs---\n");
	for(int i=0;i<nbjoueur;i++){
			printf("\n%s : %d",joueurs[i].nom, joueurs[i].tdb);
	}
}


/* Auteur : Etienne Angé */
/* Date :   17/06/2023*/
/* Résumé : affiche le classement */
/* Entrée(s) : le tableau de joueur et le nb de joueur */
/* Sortie(s) :  aucune  */
void classement(struct joueur *joueurs, int nbJoueur){
	system("clear");
	printf("\n\n       FIN\n\n     Classement\n\n");
	int nb, nbPrec, joueurNum;
	nbPrec=-1;
	nb=500;
	for(int j=0;j<nbJoueur;j++){
		for(int i=0;i<nbJoueur;i++){
			if(joueurs[i].tdb < nb && joueurs[i].tdb > nbPrec){
				joueurNum = i;
				nb = joueurs[i].tdb;
			}
		}
		printf("   %d. %s - %d TDB\n", j+1, joueurs[joueurNum].nom, joueurs[joueurNum].tdb);
		nbPrec = nb;
		nb = 500;

	}

}

