#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "game.h"
#include "interfaceTerm.h"


/* Auteur : Etienne Angé */
/* Date :   13/06/2023*/
/* Résumé : creer les cartes du jeu de 1 à 104*/
/* Entrée(s) : le tableau du paquet déjà initialisé */
/* Sortie(s) :  aucune  */
void CreationCartesPaquet(int* paquet){
	for(int i=0; i<104;i++){
		paquet[i] = i+1;
	}
}

/* Auteur : Etienne Angé */
/* Date :   13/06/2023*/
/* Résumé : échange deux cartes du paquet */
/* Entrée(s) : le tableau du paquet, et les deux rang du tableau a échanger */
/* Sortie(s) :  auncune  */
void echange(int* paquet, int a, int b){
	int temp;
	temp = paquet[a];
	paquet[a] = paquet[b];
	paquet[b] = temp;
}

/* Auteur : Etienne Angé */
/* Date :   13/06/2023*/
/* Résumé : mélange le paquet */
/* Entrée(s) : le tableau du paquet */
/* Sortie(s) :  aucune  */
void shufflePaquet(int* paquet){
	for(int i=0; i<104;i++){
		echange(paquet, i, (rand()%104));
	}
}

/* Auteur : Hugo Dury */
/* Date : 13/06/2023 */
/* Résumé : met les cartes sur le plateau au début de la partie*/
/* Entrée(s) : le paquet de carte avec le rang des cartes déja utilisé et le tableau a double entré du plateau*/
/* Sortie(s) : aucune */
void initPlateau(int* paquet, int* rang, int** plateau){
	for(int i=0;i<5; i++){
		plateau[i][0] = paquet[*rang];
		*rang = *rang + 1;
	}
	for(int i=0; i<5; i++){
		for(int j=1;j<6;j++){
			plateau[i][j] = 0;
		}
	}
}



/* Auteur : Celia Candy */
/* Date : 14/06/23 */
/* Résumé : attribue au joueur les cartes au début de la partie */
/* Entrée(s) : le tableau des joueurs, le nb de joueur, le paquet de carte et son rang */
/* Sortie(s) : aucune */
void initHand(struct joueur *joueurs, int nbJoueur, int* paquet, int* rang){
	int desordre;
	for(int j=0; j < nbJoueur;j++){
		for(int i=0; i<10;i++){
			joueurs[j].cartes[i] = paquet[*rang];
			*rang = *rang + 1;
		}

		//trie à bulle pour mettre les cartes dans l'ordre
		do{
			desordre=0;
			for(int i=0;i<9;i++){
				if(joueurs[j].cartes[i]>joueurs[j].cartes[i+1]){
					echange(joueurs[j].cartes, i, i+1);
					desordre=1;
				}
			}
		}while(desordre==1);
	}
}

/* Auteur : Etienne Angé */
/* Date :   15/06/2023 */
/* Résumé : fais choisir une carte a un joueur */
/* Entrée(s) : la structure du joueur */
/* Sortie(s) :  la carte choisie  */
int choisir(struct joueur joueurs){
	int choix;
	int j;
	showHand(joueurs);
	printf("\nQuelle cartes voulez vous jouer ?\n");
	scanf("%d", &choix);
	j=0;
	while(joueurs.cartes[j] != choix && j<10){
		j++;
	}

	while(j>=10){
		printf("Vous ne possédez pas cette carte !\n");
		printf("Quelle cartes voulez vous jouer ?\n");
		scanf("%d", &choix);

		j=0;
		while(joueurs.cartes[j] != choix && j<10){
			j++;
		}
	}

	return j;
}

/* Auteur : Hugo Dury */
/* Date : 18/06/2023 */
/* Résumé : renvoie un tableau avec les cartes que le joueur dispose (pour les IA) */
/* Entrée(s) : struct joueur du joueur concerné */
/* Sortie(s) : tableau avec les cartes disponible et dans la case 10 le nb de carte dispo */
int* carteDispo(struct joueur joueurs){
	int* tab;
	tab = malloc(11*sizeof(int));
	int nbCarte = 0;
	for(int i=0;i<10;i++){
		if(joueurs.cartes[i] != 0){
			tab[nbCarte] = i;
			nbCarte++;
		}
	}
	tab[11] = nbCarte;
	return tab;
}

/* Auteur : Celia Candy */
/* Date : 18/06/23 */
/* Résumé : Choisi une carte aléatoire pour l'ia */
/* Entrée(s) : struct joueur de l'ia */
/* Sortie(s) : le rang de la carte */
int choixCarteIa(struct joueur joueurs){
	int* tab;
	int carte;
	tab = carteDispo(joueurs);
	carte = tab[rand() % tab[11]];
	free(tab);
	return carte;
}

/* Auteur : Etienne Angé */
/* Date :   15/06/2023 */
/* Résumé : trie les joueurs en fonction de la carte qu'ils ont choisie par ordre croissant */
/* Entrée(s) : le tableau de chaque carte choisie, le tableau initialisé par un tableau de int du nombre de joueur et la structure du joueur */
/* Sortie(s) :  aucune  */
void choixTrie(int* choix, int* choixTrie, int nbJoueur){
	int nb, nbPrec;
	nbPrec=0;
	nb=105;
	int rang;
	for(int j=0;j<nbJoueur;j++){
		for(int i=0;i<nbJoueur;i++){
			if(choix[i] < nb && choix[i] > nbPrec){
				nb = choix[i];
				rang = i;
			}
		}
		choixTrie[j] = rang;
		nbPrec=nb;
		nb = 105;

	}
}



/* Auteur : Etienne Angé */
/* Date :   16/06/2023 */
/* Résumé : place les cartes des joueurs sur le plateau */
/* Entrée(s) : le tableau du plateau, la carte que le joueur veut placer et la structure du joueur  */
/* Sortie(s) :  aucune  */
void setCard(int** plateau, int carte, struct joueur *joueurs){

	int carteSet = 0;
	int ligneSelect = 5;

	for(int i=0;i<5;i++){
		if(plateau[i][plateau[i][5]] < carte && plateau[i][plateau[i][5]] > carteSet){
			ligneSelect = i;
			carteSet = plateau[i][plateau[i][5]];
		}
	}

	// si le joueur a une carte trop petite pour la placer
	if(ligneSelect == 5){
		if(joueurs->ia == 0){
			do{
				system("clear");
				showGameBoard(plateau);
				printf("\n%s sélectionnez une ligne de 1 à 5 : ", joueurs->nom);
				scanf("%d", &ligneSelect);
				ligneSelect--;
			}while(ligneSelect<0 || ligneSelect>4);
		}
		else{
			ligneSelect = rand()%5;
		}

		joueurs->tdb += penalite(plateau[ligneSelect]);
		for(int j=1;j<5;j++){
			plateau[ligneSelect][j] = 0;
		}
		plateau[ligneSelect][0] = carte;
		plateau[ligneSelect][5] = 0;
	}
	// si la carte du joueur peut être place sur une ligne avec 5 cartes
	else if(plateau[ligneSelect][5] == 4){
		joueurs->tdb += penalite(plateau[ligneSelect]);
		for(int j=1;j<6;j++){
			plateau[ligneSelect][j] = 0;
		}
		plateau[ligneSelect][0] = carte;
	}
	// si la carte peut être posé correctement
	else{
		plateau[ligneSelect][plateau[ligneSelect][5]+1] = carte;
		plateau[ligneSelect][5]++;
	}
}

/* Auteur : Celia Candy */
/* Date : 16/06/23 */
/* Résumé : donne le nb de tdb dans une ligne du plateau */
/* Entrée(s) : la ligne du plateau */
/* Sortie(s) : le nb de tdb */
int penalite(int* ligne){
	int tdb;
	int test;
	tdb = 0;
	for(int i=0;i<5;i++){
		test = 0;

		if (ligne[i] != 0){

			if (ligne[i] % 10 == 5){
			tdb = tdb + 2;
			test = 1;
			}

			else if (ligne[i] % 10 == 0){
			tdb = tdb + 3;
			test = 1;
			}

			if (ligne[i] % 10 == ligne[i] / 10){
				tdb = tdb + 5;
				test = 1;
			}
			if (test == 0){
				tdb++;
			}
		}
	}
	return tdb;
}

/* Auteur : Hugo Dury */
/* Date : 17/06/2023 */
/* Résumé : vériefie si l'un des joueur a plus de 66 tdb */
/* Entrée(s) : le tableau des joueurs et le nb de joueur */
/* Sortie(s) : 1 si la partie doit se finir, si non 0 */
int fini(struct joueur *joueurs, int nbJoueur){
	int tdbMax = 0;
	for(int i=0;i<nbJoueur;i++){
		if(joueurs[i].tdb > tdbMax){
			tdbMax = joueurs[i].tdb;
		}
	}
	if(tdbMax>66){
		return 1;
	}
	else{
		return 0;
	}

}


/* Auteur : Etienne Angé */
/* Date :   18/06/2023 */
/* Résumé : fais une pause dans le programme */
/* Entrée(s) : le nb de ms */
/* Sortie(s) :  aucune  */
void delay(int i){
    clock_t start;
    start=clock();
    while((clock()-start)<=((i*CLOCKS_PER_SEC)/1000));
}