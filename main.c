#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "game.h"
#include "interfaceTerm.h"

int main(){

	srand(time(NULL));

    //initialisation du paquet
    int* paquet;
    paquet = malloc(104 * sizeof(int));
    //valeur permettant de connaitre les sorties du paquet (rang croissant de 0 à 103)
    int* rangPaquet;
    rangPaquet = malloc(sizeof(int));
    CreationCartesPaquet(paquet);
    



    int* nbJoueur = malloc(sizeof(int));
    int* nbIa = malloc(sizeof(int));

	intro(nbJoueur, nbIa);

	struct joueur *joueurs;
    joueurs = malloc((*nbJoueur + *nbIa) * sizeof(struct joueur));

    pseudo(*nbJoueur, *nbIa, joueurs);

    //initialisation du plateau
    int** plateau;
    plateau = malloc(5 * sizeof(int*));
    for(int i=0; i<5;i++){
        plateau[i] = malloc(6 * sizeof(int));
    }


    int* choix;
    choix = malloc((*nbJoueur + *nbIa)*sizeof(int));
    //tableau avec le rang des joueur trié en fonction des cartes choisi par ordre croissant
    int* rangChoixTrie;
	rangChoixTrie = malloc((*nbJoueur + *nbIa)*sizeof(int));

	int carteRang;

	do{
		shufflePaquet(paquet);
		*rangPaquet = 0;
		initPlateau(paquet, rangPaquet, plateau);
    	initHand(joueurs, (*nbIa+*nbJoueur), paquet, rangPaquet);
		for(int k=0;k<10;k++){
			system("clear");
			showTDB(*nbJoueur+*nbIa, joueurs);
			showGameBoard(plateau);

	        // fais choisir la carte à jouer pour chaque joueur et les met dans le tableau choix
	        for(int i=0; i<*nbJoueur;i++){
	            carteRang = choisir(joueurs[i]);
	            choix[i] = joueurs[i].cartes[carteRang];
	            joueurs[i].cartes[carteRang] = 0;
				system("clear");
				showTDB(*nbJoueur+*nbIa, joueurs);
				showGameBoard(plateau);

	        }
			for(int i=*nbJoueur; i<*nbJoueur+*nbIa;i++){
	            carteRang = choixCarteIa(joueurs[i]);
	            choix[i] = joueurs[i].cartes[carteRang];
	            joueurs[i].cartes[carteRang] = 0;

	        }

	        // met a jour le tableau rangChoixTrie
	        choixTrie(choix, rangChoixTrie, *nbIa+*nbJoueur);

	    	for(int i=0;i<*nbJoueur+*nbIa;i++){
	            setCard(plateau, choix[rangChoixTrie[i]], &joueurs[rangChoixTrie[i]]);

	            system("clear");
				showTDB(*nbJoueur+*nbIa, joueurs);
				showGameBoard(plateau);
		        printf("\n\nLes cartes sont les suivantes :");
		        for(int j=0;j<*nbJoueur+*nbIa;j++){
		            printf("%d ", choix[rangChoixTrie[j]]);
		    	}
				delay(500);
	        }
	    }

   	}
   	while(fini(joueurs, *nbJoueur+*nbIa) == 0);

   	classement(joueurs, *nbJoueur+*nbIa);

   	free(paquet);
	free(rangPaquet);
	free(nbJoueur);
	free(nbIa);
	free(joueurs);
	for(int i=0;i<5;i++){
		free(plateau[i]);
	}
	free(plateau);
	free(choix);
	free(rangChoixTrie);


	return 0;
}