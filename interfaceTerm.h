#ifndef __INTERFACETERM_H_
#define __INTERFACETERM_H_

void intro(int* nbJoueur, int* nbIa);
void pseudo(int nbJoueur,int nbIa, struct joueur *joueurs);
void showGameBoard(int** plateau);
void showHand(struct joueur player);
void showTDB(int nbjoueur, struct joueur *joueurs);
void classement(struct joueur *joueurs, int nbJoueur);

#endif