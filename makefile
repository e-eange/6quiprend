exe: main.o game.o interfaceTerm.o 
	gcc main.o game.o interfaceTerm.o  -o exe

game.o: game.c game.h
	gcc -c -Wall game.c -o game.o

interface-term.o: interfaceTerm.c interfaceTerm.h
	gcc -c -Wall interfaceTerm.c -o interfaceTerm.o

main.o: main.c game.h interfaceTerm.h
	gcc -c -Wall main.c -o main.o

clean:
	rm *.o