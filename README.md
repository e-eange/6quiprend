# 6 qui prend

Jouer au 6 qui prend sur un ordinateur en local avec ou non des IA

## Pre-requis

Avoir les commandes git, make et gcc. Vous pouvez les installer avec les commandes suivante :  
__Debian ou Ubuntu :__  
```sudo apt install git```  
```sudo apt install make```  
```sudo apt install gcc```

## Installation

Télécharger les fichiers du jeu avec la commande :  
```git clone https://gitlab.etude.cy-tech.fr/e-eange/6quiprend.git```

Puis utiliser la commande ```make``` pour compiler le jeu. Puis lancer le fichier executable avec la commande ```./exe```.

## Les règles du jeu
Vous trouverez les règles du jeu [ici](https://www.regledujeu.fr/6-qui-prend/).