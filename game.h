#ifndef __GAME_H_
#define __GAME_H_

struct joueur{
	char nom[30];
	int cartes[10];
	int tdb;
	int ia;
};

void CreationCartesPaquet(int* paquet);
void echange(int* paquet, int a, int b);
void shufflePaquet(int* paquet);
void initPlateau(int* paquet, int* rang, int** plateau);
void initHand(struct joueur *joueurs, int nbJoueur, int* paquet, int* rang);
int choisir(struct joueur joueurs);
int* carteDispo(struct joueur joueurs);
int choixCarteIa(struct joueur joueurs);
void choixTrie(int* choix, int* choixTrie, int nbJoueur);
void setCard(int** plateau, int carte, struct joueur *joueurs);
int penalite(int* ligne);
int fini(struct joueur *joueurs, int nbJoueur);
void delay(int i);

#endif